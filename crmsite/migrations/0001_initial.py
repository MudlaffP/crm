# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100)),
                ('street', models.CharField(max_length=50)),
                ('house_number', models.IntegerField(validators=[django.core.validators.MinValueValidator(1)])),
                ('apartment_number', models.IntegerField(null=True, blank=True, validators=[django.core.validators.MinValueValidator(1)])),
                ('postal_code', models.CharField(max_length=6)),
                ('city', models.CharField(max_length=100)),
            ],
        ),
    ]

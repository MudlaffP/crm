from django.db import models
from django.core.validators import MinValueValidator


class Company(models.Model):
    name = models.CharField(max_length=100, unique=True)
    street = models.CharField(max_length=50)
    house_number = models.IntegerField(blank=False, validators=[MinValueValidator(1)])
    apartment_number = models.IntegerField(blank=True, null=True, validators=[MinValueValidator(1)])
    postal_code = models.CharField(max_length=6)
    city = models.CharField(max_length=100)

    def __str__(self):
        return self.name

from django.conf.urls import patterns, url
from crmsite.views import companies_view, users_view, company_create_or_update, company_save, company_details, change_user_status, create_edit_user, save_user

urlpatterns = patterns('',
                       url(r'^$', companies_view, name='companies'),
                       # AUTHORIZATION
                       url(r'^logout/$', 'django.contrib.auth.views.logout', {
                           'template_name': 'registration/logout.html'}),
                       url(r'^perm_login/$', 'django.contrib.auth.views.login', {
                           'template_name': 'registration/missing_permission.html'}),
                       #USER
                       url(r'^users/$', users_view, name='users'),
                       url(r'^user/add/$', create_edit_user, name='create_edit_user'),
                       url(r'^user/save/$', save_user, name='save_user'),
                       url(r'^user/(?P<pk>\d+)/changestatus/$', change_user_status, name='change_user_status'),
                       #COMPANY
                       url(r'^company/add/$', company_create_or_update, name='company_create_or_update'),
                       url(r'^create_company/$', company_save, name='company_save'),
                       url(r'^create_company/(\d+)/$', company_save, name='company_update'),
                       url(r'^company/(?P<pk>\d+)/$', company_details, name='company_details')
                       )

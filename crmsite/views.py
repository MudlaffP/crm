from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from crmsite.models import Company
from django.contrib.auth.models import User

@login_required
def companies_view(request):
    template_name = 'site/lists/companies.html'
    companies = Company.objects.all()
    return render(request, template_name, {'companies': companies})


@login_required
def company_create_or_update(request, pk=''):
    template_name = 'site/forms/company_form.html'
    if pk:
        company = Company.objects.get(id=pk)
    else:
        company = ''
    return render(request, template_name, {'company': company})


@login_required
def company_details(request, pk):
    template_name = 'site/details/company.html'
    company = Company.objects.get(id=pk)
    return render(request, template_name, {'company': company})

@login_required
def company_save(request, pk=''):
    p = request.POST
    print(p)
    company = Company.objects.get(id=pk) if pk != '' else Company.objects.create(house_number=int(p['house_number']))
    company.name = p['name']
    company.street = p['street']
    print("House number form: " + p['house_number'])
    # company.house_number = int(p['house_number'])
    if p['apartment_number'] != '':
        company.apartment_number = int(p['apartment_number'])
    company.postal_code = p['postal_code']
    company.city = p['city']
    company.save()

    if pk == '':
        pk = company.id

    return HttpResponseRedirect(reverse('crmsite:company_details', args=[pk]))


@login_required
def users_view(request):
    template_name = 'site/lists/users.html'
    users = User.objects.all()
    return render(request, template_name, {'users': users})


@login_required
def change_user_status(request, pk):
    user = User.objects.get(id=pk)
    user.is_active = not user.is_active
    user.save()
    return HttpResponseRedirect(reverse('crmsite:users', args=[]))


@login_required
def create_edit_user(request):
    template_name = 'site/forms/user_form.html'
    return render(request, template_name, {})

@login_required
def save_user(request):
    p = request.POST
    user = User.objects.create_user(username=p['username'], email=None, password=p['password'], first_name=p['first_name'], last_name=p['last_name'])
    return HttpResponseRedirect(reverse('crmsite:users', args=[]))
